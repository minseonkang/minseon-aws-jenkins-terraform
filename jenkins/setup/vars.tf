variable "AWS_REGION" {
  default = "ap-northeast-2"
}

variable "PATH_TO_PRIVATE_KEY" {
  default = "C:/Users/minseon/.ssh/id_rsa"
}

variable "PATH_TO_PUBLIC_KEY" {
  default = "C:/Users/minseon/.ssh/id_rsa.pub"
}

variable "AMIS" {
  type = map(string)
  default = {
    ap-northeast-2 = "ami-0a25005e83c56767a"
    us-east-1      = "ami-0f9cf087c1f27d9b1"
    us-west-2      = "ami-0653e888ec96eab9b"
    eu-west-1      = "ami-09f0b8b3e41191524"
  }
}

variable "INSTANCE_DEVICE_NAME" {
  default = "/dev/xvdh"
}

variable "JENKINS_VERSION" {
  default = "2.222.4"
}

variable "TERRAFORM_VERSION" {
  default = "0.12.26"
}

variable "APP_INSTANCE_COUNT" {
  default = "0"
}

