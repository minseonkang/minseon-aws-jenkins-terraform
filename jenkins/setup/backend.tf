terraform {
  backend "s3" {
    bucket = "minseon2-aws-jenkins-terraform"
    key    = "minseon2-aws-jenkins-terraform.tfstate"
    region = "ap-northeast-2"
  }
}

